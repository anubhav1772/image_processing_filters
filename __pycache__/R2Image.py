import math
import numpy as np
from PIL import Image
from R2Pixel import R2Pixel
import matplotlib.pyplot as plt


class Filters(R2Pixel):
    def __init__(self, image_name):
        R2Pixel.__init__(self, image_name)              # Alternatively, use super().__init__(self, image_name)
        
    def brighten(self, factor):
        """
        Brighten the image by multiplying each pixel component by the factor,
        then clamping the result to a valid range.
        """

        img = np.array(self.image)
        for i in range(self.channels):
            try:
                img[:,:,i] = img[:,:,i].clip(0, 255.0 / factor) * factor
            except ZeroDivisionError:
                img[:,:,i] = img[:,:,i] * factor        # when factor is 0.0

        # img = img.astype(np.uint8)
        img = Image.fromarray(img)
        self.save_image(img, 'output/princeton_small_brightness_'+str(factor)+'.jpg')
    
    def changeContrast(self, factor):
        """
        Change the contrast of an image by interpolating between the image
        and a constant gray image with the average luminance.
        Interpolation reduces constrast, extrapolation boosts constrast,
        and negative factors generate inverted images.
        """
        
        img = np.array(self.image)
        avgLumi = 0
        for i in range(self.height):
            for j in range(self.width):
                avgLumi += self.luminance(img, i, j)
        avgLumi /= self.width * self.height
        # print(avgLumi)
        pixel = np.array([avgLumi, avgLumi, avgLumi, 255])
        
        for i in range(self.height):
            for j in range(self.width):
                img[i, j] = np.clip((1-factor)*pixel + factor*img[i, j], 0.0, 255.0)
        
        img = Image.fromarray(img)
        self.save_image(img, 'output/c_contrast_'+str(factor)+'.jpg')
    
    def blur(self, sigma):
        """
        Blur an image with a Gaussian filter with a given sigma.
        Gaussian function used: 
            G(x) = exp(-x^2/(2*sigma^2))
        Gaussian filter width:
            ceil(3*sigma)*2+1
        """
        img = np.array(self.image)[:,:,:3]
        size = math.ceil(3*sigma)*2+1
        pad = (size-1)//2
        img = np.pad(img, [(pad, pad), (pad, pad), (0, 0)], mode='constant', constant_values=0)

        gaussianKernel = np.array([[0 for i in range(size)] for j in range(size)])
        for i in range(size):
            for j in range(size):
                gaussianKernel[i][j] = math.exp(-1*(i**2+j**2)/(2*sigma**2))
        gaussianKernel = gaussianKernel/(2.0 * np.pi * sigma**2)
        # print(gaussianKernel.shape)

        output = np.zeros((self.height, self.width, 3), dtype="float32")
        
        for y in range(pad, self.height+pad):
            for x in range(pad, self.width+pad):
                roi_r = img[(y-pad):(y+pad+1), (x-pad):(x+pad+1), 0]
                roi_g = img[(y-pad):(y+pad+1), (x-pad):(x+pad+1), 1]
                roi_b = img[(y-pad):(y+pad+1), (x-pad):(x+pad+1), 2]
                k_r = (roi_r * gaussianKernel).sum()
                k_g = (roi_g * gaussianKernel).sum()
                k_b = (roi_b * gaussianKernel).sum()

                output[y - pad, x - pad, 0], output[y - pad, x - pad, 1], output[y - pad, x - pad, 2] = k_r, k_g, k_b
        
        output = np.clip(output, 0.0, 255.0)
        return output.astype("uint8")
        # output = Image.fromarray(output.astype("uint8"))
        # self.save_image(output, 'output/princeton_small_blur_'+str(sigma)+'.jpg')
    
    def sharpen(self):
        img = np.array(self.image)[:,:,:3]
        blurred_img = self.blur(2.0)
        factor = 2.0
        img[:, :] = np.clip((1-factor)*blurred_img[:, :] + factor*img[:, :], 0.0, 255.0)
        # for i in range(self.height):
        #     for j in range(self.width):
        #         print(img[i, j], blurred_img[i, j])
        #         img[i, j] = np.clip((1-factor)*blurred_img[i, j] + factor*img[i, j], 0.0, 255.0)
        
        img = Image.fromarray(img)
        self.save_image(img, 'output/princeton_small_sharpen_.jpg')
        


        
        # for x0 in range(self.height):                               # no. of rows
        #     for y0 in range(self.width):                            # no. of columns
        #         pixel = np.array([0.0, 0.0, 0.0, img[x0, y0, 3]])
                
        #         total=0

        #         xl = (x0-size)
        #         xh = (x0+size+1)
        #         yl = (y0-size)
        #         yh = (y0+size+1)

        #         if xl<0:
        #             xl = 0
        #         if xh>self.height:
        #             xh = self.height
        #         if yl<0:
        #             yl = 0
        #         if yh>self.width:
        #             yh = self.width
                
                
        #         for i in range(xl, xh):
        #             for j in range(yl, yh):
        #                 g = gaussianKernel[abs(i-x0)][abs(j-y0)]
        #                 print(img[i, j])
                        # pixel += g*img[i, j]
                        # print(pixel)


        # img = Image.fromarray(img)
        # self.save_image(img, 'output/princeton_small_'+str(sigma)+'.jpg')
    
    # def detectEdge(self)

# x = Filters("input/princeton_small.jpg")
# x.brighten(0.0)
# x.brighten(0.5)
# x.brighten(2.0)

# x = Filters("input/c.jpg")
# x.changeContrast(-0.5)
# x.changeContrast(0.0)
# x.changeContrast(0.5)
# x.changeContrast(2.0)

# x = Filters("input/c.jpg")
# x.blur(0.125)
# x.blur(2)
# x.blur(8)

x = Filters("input/c.jpg")
x.sharpen()

# x = Filters("input/c.jpg")
# x.detectEdge(0.125)


# print(x.channels)
# print(x.get_pixel(100, 90))

